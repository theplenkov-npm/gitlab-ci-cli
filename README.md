# Gitlab CI/CD command line 

This project is a monorepo for following projects:

- [@gitlaber/cli](https://www.npmjs.com/package/@gitlaber/cli) - CLI with utilities for Gitlab CI/CD
- [@gitlaber/sdk](https://www.npmjs.com/package/@gitlaber/sdk) - SDK with functions used by CLI
- [@gitlaber/cascade](https://www.npmjs.com/package/@gitlaber/cascade) - `gitlab cascade` command to create/update cascaded MRs
- [@gitlaber/wait-for](https://www.npmjs.com/package/@gitlaber/wait-for) - `gitlab wait` command to wait for another pipelines

## CI/CD 

This project is build using nx framework. The versioning logic is following:
- new version is triggered with a standard `npm version` command for a root package
- if subpackgage is updated since previous root version - it will inherit same number
- so in any subpackage version we can clearly see in which monorepo release it has the latest change
- versions are published in Gitlab CI/CD with internal `publish-all-projects` command
- this command analyses all existing workspaces versions( nx projects ) and in case they are not yet published - publishes them



