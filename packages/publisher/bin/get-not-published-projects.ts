#!/usr/bin/env ts-node
import { echo } from 'zx';
import { getNotPublishedProjects } from '../src/index';

// prints not published projects
echo(await getNotPublishedProjects());
