#!/usr/bin/env ts-node
import { $, echo } from 'zx';
import { getNotPublishedProjects } from '../src/index';

const projects = (await getNotPublishedProjects()).join(',');

(projects?.length &&
  (await $`npx nx run-many --target=publish --projects=${projects}`)) ||
  echo('No projects to publish found');
