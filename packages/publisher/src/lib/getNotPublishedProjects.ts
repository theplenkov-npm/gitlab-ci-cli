import { $ } from 'zx';
import { importJSON } from './importJson';
import path from 'path';

// doesn't work in ts-node
// import { projects } from 'workspace.json';
type PackageJSON = { name: string; version: string; private?: boolean };
type WorkspacesJSON = { projects: Record<string, string> };

export async function getNotPublishedProjects() {
  const { projects } = (await importJSON('workspace.json')) as WorkspacesJSON;

  // read packages
  const pkgs = await Promise.all(
    Object.entries(projects).map(async ([project, _path]) => {
      const pkg: PackageJSON = await importJSON(
        path.resolve(_path, 'package.json')
      );
      return { project, pkg };
    })
  );

  // exclude private ones
  const publicPkgs = pkgs.filter(({ pkg }) => !pkg.private);

  // read npm view ( check if it's published )
  const published = await Promise.all(
    publicPkgs.map(async ({ project, pkg }) => {
      const { name, version } = pkg;
      try {
        await $`npm view ${name}@${version} --json`.quiet();
        return { project, published: true };
      } catch (error) {
        return { project, published: false };
      }
    })
  );

  return published
    .filter(({ published }) => !published)
    .map(({ project }) => project);
}
