import { readFile } from 'node:fs/promises';
import path from 'node:path';
import { cwd } from 'node:process';

export async function importJSON(_path: string) {    
  const file = await readFile(path.resolve(cwd(), _path));
  return JSON.parse(file.toString());
}
