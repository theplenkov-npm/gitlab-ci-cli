import { Command } from 'commander';
import { cascade } from '@gitlaber/cascade';
import { wait } from '@gitlaber/wait-for';

export const gitlab = new Command();

gitlab.addCommand(cascade);
gitlab.addCommand(wait);
