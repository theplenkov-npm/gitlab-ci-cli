# Gitlab CI/CD command line

This command line is intended to keep helper funtions which are not available in glab official CLI but might be useful in some CI/CD templates

# Usage

```
Usage: gitlab [options] [command]

Options:
  -h, --help         display help for command

Commands:
  cascade [options]  Create/Update cascaded merge request
  wait [options]     Wait for another pipeline
  help [command]     display help for command
```

## Commands

### cascade: cascade merge requests
```
Usage: gitlab cascade [options]

Create/Update cascaded merge request

Options:
  --project <projectPath>      Gitlab project path (default $CI_PROJECT_PATH) (default: "/theplenkov-npm/gitlab-ci-cli")
  --source <sourceBranch>      Source branch (default $CI_COMMIT_BRANCH) (default: "main")
  --target <targetBranch>      Target branch
  --title <title>              Merge request title
  --description <description>  Merge request description
  -h, --help                   display help for command
```
### wait

```
Usage: gitlab wait [options]

Wait for another pipeline

Options:
  --project <projectPath>  Gitlab project path (default $CI_PROJECT_PATH) (default: "/theplenkov-npm/gitlab-ci-cli")
  --ref <pipelineRef>      Pipeline reference
  --delay <delayInSec>     Poll every <delay> seconds (1 second by default) (default: "1")
  -h, --help               display help for command
```
