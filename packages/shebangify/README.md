# shebangify
A command line wrapper to insert shebang

## Usage
```
shebangify <fileName> <program>
# For example
shebangify bin/run.ts ts-node
shebangify bin/run.js node
```


