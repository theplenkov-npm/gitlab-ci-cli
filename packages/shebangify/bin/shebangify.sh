#!/usr/bin/env bash
[ "$(head -c 2 $1)" = "#!" ] && sed -i "1 s/^.*$/#!\/usr\/bin\/env $2/" $1 || sed  -i "1i #!/usr/bin/env $2" $1