# Generated SDK for GraphQL API

Contains only needed queries and mutations.

## How to extend

- add new command to `commands` folder
- run `graph-codegen` command again
- [sdk.ts file](src/generated/sdk.ts) will be regenerated
- it's not stored in the source code - but is available as job artifact and also published on npm
