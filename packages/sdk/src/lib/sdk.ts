import { getSdk } from '../generated/sdk';
import { GraphQLClient } from 'graphql-request';
import { env } from 'node:process';

const { CI_SERVER_URL, GITLAB_TOKEN } = env;

export function getClient() {
  const client = new GraphQLClient(`${CI_SERVER_URL}/api/graphql`, {
    headers: {
      authorization: `Bearer ${GITLAB_TOKEN}`,
    },
  });

  return getSdk(client);
}
