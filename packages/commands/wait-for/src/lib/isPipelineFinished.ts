import { getClient, GetRefPipelinesQueryVariables, PipelineStatusEnum } from "@gitlaber/sdk";

export async function isPipelineFinished(input: GetRefPipelinesQueryVariables) {

  let result = await getClient().getRefPipelines(input);

  let pipelines = result?.project?.pipelines?.nodes;    

  if (!pipelines || !pipelines.length) {
    throw `No pipelines have been found for ref ${input.ref}`;
  } else if (    
    pipelines?.some(
      (pipeline) => pipeline?.status === PipelineStatusEnum.SUCCESS
    )
  ) {
    return true;
  } else if (pipelines?.some((pipeline) => pipeline?.active)) {
    // prevent multiple entries
    return false;
  } else {
    throw `Pipeline ${input.ref} has failed or finished with unexpected status`;
  }   
}


