//start polling each second

import { isPipelineFinished } from './isPipelineFinished';

export function waitForPipeline(
  input: Parameters<typeof isPipelineFinished>[0],
  delayInSec = 1
) {
  // main timer
  const interval = setInterval(async () => {  
    try {

      if (await isPipelineFinished(input)) {
        console.log(`✅ pipeline ${input.ref} is finished`);
        clearInterval(interval);
      } else {
        // prevent multiple entries
        process.stdout.write('⏳ Waiting for a tag pipeline to be finished...\r');
      }
      
    } catch (error) {

      const message = (error as Error).message || typeof error === "string" && error;

      throw message && `❌ ${message}` || error;
      
    }      
    
  }, 1000 * delayInSec);
}
