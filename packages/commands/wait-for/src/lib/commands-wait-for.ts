import { Command } from 'commander';
import { env, exit } from 'node:process';
import { isPipelineFinished } from './isPipelineFinished';
import { setTimeout } from 'node:timers/promises';

const { CI_PROJECT_PATH } = env;

export const wait = new Command('wait');

wait
  .description('Wait for another pipeline')
  .option(
    '--project <projectPath>',
    'Gitlab project path (default $CI_PROJECT_PATH)',
    CI_PROJECT_PATH
  )
  .option('--ref <pipelineRef>', 'Pipeline reference')
  .option(
    '--delay <delayInSec>',
    'Poll every <delay> seconds (1 second by default)',
    '1'
  )
  .action(async ({ project, ref, delay }) => {
    function* poll() {
      while (true) {
        yield isPipelineFinished({ projectPath: project, ref });
      }
    }

    try {
      for await (const finished of poll()) {
        if (finished) {
          console.log(`\n✅ pipeline ${ref} is finished`);
          exit(0);
        } else {
          // prevent multiple entries
          process.stdout.write(
            '⏳ Waiting for a tag pipeline to be finished...\r'
          );
          await setTimeout(delay);
        }
      }
    } catch (error) {
      const message =
        (error as Error).message || (typeof error === 'string' && error);

      if (message) {
        console.error(`❌ ${message}`);
        process.exit(-1);
      }

      throw error;
    }
  });
