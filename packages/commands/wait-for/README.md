# Wait for another pipeline in Gitlab CI/CD
Pretend we have the situation when we use tag pipelines to build some artifacts like packages or images. Using tags guarantees uniqueness also they can be protected so it's a quick win for many compliance controls. However, pretend you have a deployment ( especially using gitlab flow with environment branches ) via MRs. So it may happen quite easily that by moment when MR got approved Tag pipeline is not yet finished and artifacts are not yet fully build so deployment is not ready. So it would be nice to have something like this `gitlab wait --ref v${npm_package_version}`.

## Usage

```
Usage: wait [options]

Wait for another pipeline

Options:
  --project <projectPath>  Gitlab project path (default $CI_PROJECT_PATH) (default: "/theplenkov-npm/gitlab-ci-cli")
  --ref <pipelineRef>      Pipeline reference
  --delay <delayInSec>     Poll every <delay> seconds (1 second by default) (default: "1")
  -h, --help               display help for command
```

| This command is a part of @giltaber/cli CLI
