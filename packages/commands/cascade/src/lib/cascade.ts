import { getClient, MergeRequestState } from "@gitlaber/sdk";

const CASCADED = "cascaded";

export async function cascadeMergeRequest({
  projectPath,
  sourceBranch,
  targetBranch,
  title,
  description,
}: {
  projectPath: string;
  sourceBranch: string;
  targetBranch: string;
  title: string;
  description: string;
}) {
  const client = getClient();

  const openMergeRequests = await client.getProjectMergeRequests({
    projectPath,
    sourceBranches: [sourceBranch],
    targetBranches: [targetBranch],
    state: MergeRequestState.opened,
  });

  if (openMergeRequests?.project?.mergeRequests?.nodes?.length) {
    // update only cascaded MRs
    openMergeRequests?.project?.mergeRequests?.nodes?.forEach(
      async (mr) =>
        mr?.labels?.nodes?.find((label) => label?.title === CASCADED) &&
        (await client.mergeRequestUpdate({
          input: {
            iid: mr.iid,
            projectPath,
            title,
            description,
          },
        }))
    );
  } else {
    // create cascaded MR
    await client.mergeRequestCreate({
      input: {
        projectPath,
        sourceBranch,
        targetBranch,
        title,
        description,
        labels: [CASCADED],
      },
    });
  }
}
