import { Command } from 'commander';
import { cascadeMergeRequest } from './cascade';
import assert from 'node:assert/strict';

type Opts = {
  project: string;
  source: string;
  target: string;
  title: string;
  description: string;
};

import { env } from 'node:process';
const { CI_PROJECT_PATH, CI_COMMIT_BRANCH } = env;

export const cascade = new Command('cascade');

cascade
  .description('Create/Update cascaded merge request')
  .option(
    '--project <projectPath>',
    'Gitlab project path (default $CI_PROJECT_PATH)',
    CI_PROJECT_PATH
  )
  .option(
    '--source <sourceBranch>',
    'Source branch (default $CI_COMMIT_BRANCH)',
    CI_COMMIT_BRANCH
  )
  .option('--target <targetBranch>', 'Target branch')
  .option('--title <title>', 'Merge request title')
  .option('--description <description>', 'Merge request description')
  .action(({ project, source, target, title, description }: Opts) => {
    try {
      assert(
        project,
        'Gitlab project path is not provided neither in --project parameter nor via CI_PROJECT_PATH environment variable'
      );
      return cascadeMergeRequest({
        projectPath: project,
        sourceBranch: source,
        targetBranch: target,
        title,
        description,
      });
    } catch (error: unknown) {
      if (error instanceof Error) {
        console.error(error.message);
        process.exit(1);
      }
      throw error;
    }
  });
