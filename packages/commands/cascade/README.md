# CLI command implementing cascaded merge requests

The idea of this command came from couple feature proposals
- [Cascaded Merge Requests proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/11648)
- [Inability to create MRs from the default branch using push options](https://gitlab.com/gitlab-org/gitlab/-/issues/214123)

Behind both of features I have in mind Gitlab Flow with [environment branches approach](https://docs.gitlab.com/ee/topics/gitlab_flow.html#environment-branches-with-gitlab-flow)

So this approach suggests moving changes to different environments by use of MRs. With numerous environments it may require large number of MRs to be created and I thought that it would be nice to automate MR creation/update process

## Usage
```
Usage: gitlab cascade [options]

Create/Update cascaded merge request

Options:
  --project <projectPath>      Gitlab project path (default $CI_PROJECT_PATH)
  --source <sourceBranch>      Source branch (default $CI_COMMIT_BRANCH)
  --target <targetBranch>      Target branch
  --title <title>              Merge request title
  --description <description>  Merge request description
  -h, --help                   display help for command
```

This command has the following logic:
- It tries to find the existing open merge requests between source and target branches
- In case if there are no requests - it will create a new MR labeled as cascaded
- In case if there are MRs - it will try to update existing MR's( only those labeled with cascaded ) with the provided title and description

This project also delivers an approximate [template](.gitlab/cascade.gitlab-ci.yml) how this feature can be used.

| This command is a part of @giltaber/cli CLI
